#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

#include "GenericPlayer.h"

using namespace std;

class Player : public GenericPlayer
{
public:
	Player(const string& name = "");

	virtual ~Player();

	//returns whether or not the player wants another hit       
	virtual bool IsHitting() const;

	//announces that the player wins
	void Win(int winType);

	//announces that the player loses
	int Lose() const;

	//announces that the player pushes
	void Push() const;

	//placing a bet
	void PlaceBet();

	//adding to their bet
	void AddToBet();

protected:
	int m_betAmount;
};
#endif // !PLAYER_H

