#ifndef HOUSE_H
#define HOUSE_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

#include "GenericPlayer.h"

using namespace std;

class House : public GenericPlayer
{
public:
	House(const string& name = "House",int amount = 500);

	virtual ~House();

	//indicates whether house is hitting - will always hit on 16 or less
	virtual bool IsHitting() const;

	//flips over first card
	void FlipFirstCard();
	//updates the balance
	void addBalance(int bal);

protected:
	int m_Balance;
	//this is so that the houses balance can be added to
	//so that any won bets can increase it
};

#endif // !HOUSE_H
