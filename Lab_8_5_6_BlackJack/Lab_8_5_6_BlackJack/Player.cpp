#include "Player.h"
#include "House.h"



Player::Player(const string& name) :
	GenericPlayer(name)
{}

Player::~Player()
{}

bool Player::IsHitting() const
{
	cout << m_Name << ", do you want a hit? (Y/N): ";
	char response;
	cin >> response;
	return (response == 'y' || response == 'Y');
}

void Player::Win(int winType)
{
	cout << m_Name << " wins.\n";
	if (winType == 1)
	{
		cout << "you win this amount: " << m_betAmount << "\n";
		m_balance += m_betAmount;
		cout << "your new balance is: " << m_balance << "\n";
	}
	else if(winType == 2)
	{
		m_betAmount = m_betAmount * 2;
		cout << "you win this amount: " << m_betAmount << "\n";
		m_balance += m_betAmount;
		cout << "your new balance is: " << m_balance << "\n";
	}
	else if (winType == 3)
	{
		m_betAmount = m_betAmount * 3;
		cout << "you win this amount: " << m_betAmount << "\n";
		m_balance += m_betAmount;
		cout << "your new balance is: " << m_balance << "\n";
	}
}

int Player::Lose() const
{
	cout << m_Name << " loses.\n";
	cout << "You bet " << m_betAmount << "\n";
	cout << "your new balance is: " << m_balance << "\n";
	return m_betAmount;


}

void Player::Push() const
{
	cout << m_Name << " pushes.\n";

}
void Player::PlaceBet()
{	
	cout << "How much does " << m_Name << " want to bet? : ";
	//
	//code in a check to make sure that the ammount is within acceptable
	//parameters ie the 20 maximum limit
	//
	cin >> m_betAmount;
	m_balance = m_balance - m_betAmount;
	
}
void Player::AddToBet()
{
	char a;
	cout << "would you like to add to bet " << m_Name << "\n Y to place bet, N to not: ";
	cin >> a;
	if (a == 'y' || a == 'Y')
	{
		int newbet;
		cout << "How much does " << m_Name << " want to bet? ";

		cin >> newbet;
		m_betAmount += newbet;
		m_balance = m_balance - newbet;
	}
	else if(a == 'n' || a == 'N')
	{
		cout << "Thank you!";
	}

}
