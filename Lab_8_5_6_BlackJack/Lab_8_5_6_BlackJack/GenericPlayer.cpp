#include "GenericPlayer.h"

GenericPlayer::GenericPlayer(const string& name,int amount) :
	m_Name(name),
	m_balance(amount)
{}

GenericPlayer::~GenericPlayer()
{}

bool GenericPlayer::IsBusted() const
{
	return (GetTotal() > 21);
}

void GenericPlayer::Bust() const
{
	cout << m_Name << " busts.\n";
}