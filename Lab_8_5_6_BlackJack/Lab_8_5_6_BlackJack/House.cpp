#include "House.h"
#include "GenericPlayer.h"

House::House(const string& name,int amount) :
	GenericPlayer(name,amount)
{}

House::~House()
{}

bool House::IsHitting() const
{
	return (GetTotal() <= 16);
}

void House::FlipFirstCard()
{
	if (!(m_Cards.empty()))
	{
		m_Cards[0]->Flip();
	}
	else
	{
		cout << "No card to flip!\n";
	}
}
void House::addBalance(int bal)
{
	// takes in the losing players bet and adds to the house stockpile
	m_balance += bal;
	cout << "house gains: " << bal << "\n";
	cout << "house's new balance is: " << m_balance << "\n";
}
