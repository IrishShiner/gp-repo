#include "Enemy.h"
#include "GameObject.h"
#include <ctime>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>



Enemy::Enemy()
{
	
	//selects a random point on the map
	int Xpoint = createCoordX();
	
	
	
	int Ypoint = createCoordY();
	// creates the enemy
	spawn("Enemy",m_health, m_speed, Xpoint, Ypoint);
}
Enemy::~Enemy()
{

}

void Enemy::update()
{
	//creates the random and prints it
	
	int randomMove = rand() % 4;
	cout << "random is: " << randomMove << endl;

	//depending on what the random is the direction the enemy will move
	if (randomMove == 0)
	{
		if (m_X - 1 < 0)
		{
			m_X = 0;
			cout << "Cannot move in this direction" << endl;
			

		}
		else
		{
			m_X = m_X - 1;
			
		}
	}
	else if (randomMove == 1)
	{
		if (m_X + 1 > 20)
		{
			m_X = 20;
			cout << "Cannot move in this direction" << endl;

		}
		else
		{
			m_X = m_X + 1;
			
		}
	}
	else if (randomMove == 2)
	{
		if (m_Y + 1 > 20)
		{
			m_Y = 20;
			cout << "Cannot move in this direction" << endl;
			

		}
		else
		{
			m_Y = m_Y + 1;
		}
			
	}
	else if (randomMove == 3)
	{
		if (m_Y - 1 < 0)
		{
			m_Y = 0;
			cout << "Enemy Cannot move in this direction" << endl;
			

		}
		else
		{
			m_Y = m_Y - 1;
			
		}
	}
	//takes health from enemy
	m_health = m_health - (m_speed * 2);
	//tells you what health is now
	cout << m_typeID << "'s health has decreased to: " << m_health << endl;
	//tells you the enemys new location
	cout << m_typeID << " has moved to location " << m_X << "," << m_Y << endl;
}