#include <iostream>
#include <vector>
#include "Enemy.h"
#include "Game.h"
#include "Player.h"
#include "GameObject.h"
#include <ctime>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>


using namespace std;


int main()
{
	srand(static_cast<unsigned int>(time(NULL)));
	cout << "Welcome to the game\n\n\n\n"<< endl;

	Game theGame = Game();

	theGame.init();
	theGame.fill();
	int gameLoop = 0;

	while(gameLoop < 20 && !theGame.getGameState())
	{
		theGame.printMap();
		theGame.draw();
		theGame.info();
		theGame.battle();
		theGame.isAlive();
		theGame.clean();

		gameLoop++;
	}

	cout << "20 rounds has passed" << endl;
	cout << "Game end" << endl;
	//theGame.info();
	//theGame.clean();

	
	getchar();


}
