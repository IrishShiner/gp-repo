#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <iostream>
#include <string>



using namespace std;

class GameObject
{
public:
	GameObject();
	GameObject(const string& typeID, int health, int speed, int X, int Y);
	virtual ~GameObject();

	void spawn(const string& m_typeID, int m_health, int m_speed, int m_X, int m_Y);
	void draw();
	virtual void update();
	void info();
	bool isAlive();
	
	int getX();
	int getY();
	string getID();
	int createCoordX();
	int createCoordY();
	int getHealth();
	void setHealth();


protected:
	string m_typeID;
	int m_health =160;
	int m_speed;
	int m_X;
	int m_Y;
	

};

#endif // !GAMEOBJECT_H
