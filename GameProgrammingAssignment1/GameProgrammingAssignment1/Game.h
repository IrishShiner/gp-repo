#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <string>
#include <vector>
#include "GameObject.h"

using namespace std;

class Game : public GameObject
{
public:
	Game();
	~Game();
	void init();
	void draw();
	void update();
	void battle();
	void info();
	void clean();
	void fill();
	void printMap();

	bool getGameState();
	bool isGameRunning;




protected:
	vector<GameObject*> vpGameObject;
	vector<GameObject*>::iterator  iter;
	char map[21][21];
	int newX;
	int newY;
	


};

#endif // !GAME_H
