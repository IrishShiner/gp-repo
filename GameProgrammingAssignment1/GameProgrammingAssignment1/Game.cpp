#include "Game.h"
#include "GameObject.h"
#include "Enemy.h"
#include "Player.h"
#include <vector>
#include <ctime>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>


Game::Game()
{
	isGameRunning = false;
}
Game::~Game()
{
	
}

void Game::init()
{
	GameObject* Player1 = new Player();
	GameObject* Enemy1 = new Enemy();
	GameObject* Enemy2 = new Enemy();
 	GameObject* Enemy3 = new Enemy();
	GameObject* Enemy4 = new Enemy();
	
	vpGameObject.push_back(Player1);
	vpGameObject.push_back(Enemy1);
	vpGameObject.push_back(Enemy2);
	vpGameObject.push_back(Enemy3);
	vpGameObject.push_back(Enemy4);
}

void Game::draw()
{
	cout << "draw function" << endl;
	for (iter =vpGameObject.begin(); iter != vpGameObject.end(); ++iter)
	{
		
	//	(vpGameObject)->draw(*iter);
		(*iter)->draw();
	}
}

void Game::update()
{
	//calls the update functions of the game objects
	for (iter = vpGameObject.begin(); iter != vpGameObject.end(); ++iter)
	{
		(*iter)->update();
	}
}

void Game::battle()
{
	int xCheck;
	int yCheck;
	int healthPlayer;
	int healthEnemy;
	int playerPoint = 1;
	// Iterate over all objects
		// If that object is player store coords in temp
		// For all other objects, if that object is an enemy and also at the coords stored in temp
		// take all hp away from that enemy based on something
	for (iter = vpGameObject.begin(); iter != vpGameObject.end(); ++iter)
	{
		string newID = (*iter)->getID();
		if (newID == "Player")
		{
			xCheck = (*iter)->getX();
			yCheck = (*iter)->getY();
			healthPlayer = (*iter)->getHealth();
		}
		else if (newID == "Enemy")
		{
			if (xCheck == (*iter)->getX() && xCheck == (*iter)->getX())
			{
				cout << "battle has begun" << endl ;
				healthEnemy = (*iter)->getHealth();
					if (healthPlayer >= healthEnemy)
					{
						(*iter)->setHealth();
					}
					else
					{
						vpGameObject.front()->setHealth();
						isGameRunning = true;
					}
				
			}
			
		}
	}
}

void Game::info()
{
	//function to run through the info in the game objects
	
		for (iter = vpGameObject.begin(); iter != vpGameObject.end(); ++iter)
		{
		(*iter)->info();
	}
}


void Game::clean()
{
	vector <GameObject*>::iterator iter;

	for (iter = vpGameObject.begin(); iter != vpGameObject.end(); )
	{
		if (((*iter)->isAlive() == true))
		{
			delete (*iter); // free memory
			iter = vpGameObject.erase(iter); // remove from vector
		}
		else
		{
			++iter; // vector is incremented
		}
	}
}

void Game::fill()
{
	//initial fill of map with *
	for (int i = 0; i < 21; i++)
	{
		for (int j = 0; j < 21; j++)
		{
			map[i][j] = '*';
		}
	}

}
void Game::printMap()
{
	fill();
	
	//iterator to go through the vpGameObject vectors
	for (iter = vpGameObject.begin(); iter != vpGameObject.end(); ++iter)
	{
		
		string newID = (*iter)->getID();
		newX = (*iter)->getX();
		newY = (*iter)->getY();

		//if the id is player or enemy it will place a p or E
		if (newID == "Player")
		{
			
			
			map[newX][newY] = 'P';
		}
		else if(newID == "Enemy")
		{
			
			map[newX][newY] = 'E';
		}
	}
	
	for (int i = 0; i < 21; i++)
	{
		for (int j = 0; j < 21; j++)
		{
			cout << map[i][j] << " ";
		}
		cout << endl;
	}

	
}

bool Game::getGameState()
{
	return isGameRunning;
}
