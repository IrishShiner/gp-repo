#include "Player.h"
#include "GameObject.h"
#include "Game.h"

Player::Player()
{
	// need to fix to work our the player location 
	spawn("Player",m_health,m_speed,0,0);
	//GameObject("Playerboy,", m_health, m_speed, 1, 2);
}

Player::~Player()
{

}

void Player::update()
{
	string input;
	bool check = false;
	cout << "What direction do you want to go?\nw = UP, s = Down, d = RIGHT, a = LEFT \n";
		cin >> input;
		while (check == false)
		{
			if (input == "w")
			{
				if (m_X - 1 < 0)
				{
					m_X = 0;
					cout << "Cannot move in this direction" << endl;
					check = true;

				}
				else
				{
					m_X = m_X - 1;
					check = true;
				}
			}
			else if (input == "s")
			{
				if (m_X + 1 > 20)
				{
					m_X = 20;
					cout << "Cannot move in this direction" << endl;
					check = true;

				}
				else
				{
					m_X = m_X + 1;
					check = true;
				}
			}
			else if (input == "d")
			{
				if (m_Y + 1 > 20)
				{
					m_Y = 20;
					cout << "Cannot move in this direction" << endl;
					check = true;

				}
				else
				{
					m_Y = m_Y + 1;
					check = true;
				}
			}
			else if (input == "a")
			{
				if (m_Y - 1 < 0)
				{
					m_Y = 0;
					cout << "Cannot move in this direction" << endl;
					check = true;

				}
				else
				{
					m_Y = m_Y - 1;
					check = true;
				}
			}
			else
			{
				cout << " incorrect input\nTry again" << endl;
			}
			m_health = m_health - (m_speed * 2);
			cout << m_typeID << "'s health has decreased to: " <<m_health << endl;
			cout << m_typeID << " has moved to location " << m_X << "," << m_Y << endl;
			
			getchar();
		}
}