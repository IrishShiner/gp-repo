#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>
#include "GameObject.h"

using namespace std;

class Player : public GameObject
{
public:
	//constructor
	Player();
	virtual ~Player();
	
	//update function
	virtual void update();

protected:
	
	
	
};
#endif // !PLAYER_H
