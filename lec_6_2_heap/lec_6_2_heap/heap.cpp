// Heap
// Demonstrates dynamically allocating memory

#include <iostream>

using namespace std;

int* intOnHeap();  //returns an int on the heap
void leak1();      //creates a memory leak
void leak2();      //creates another memory leak

int main()
{
    int* pHeap = new int;
    *pHeap = 10;
    cout << "*pHeap: " << *pHeap << "\n\n";
	cout << " pHeap mem loc is : " << pHeap << "\n\n";
    
    int* pHeap2 = intOnHeap();
    cout << "*pHeap2 which recieved pTemp : " << *pHeap2 << "\n\n";
    
    cout << "Freeing memory pointed to by pHeap.\n\n";
    delete pHeap;

    cout << "Freeing memory pointed to by pHeap2.\n\n";
    delete pHeap2;
    
    //get rid of dangling pointers
    pHeap = 0; 
    pHeap2 = 0;
	leak1();
	leak2();
	getchar();
    return 0;

	
}

int* intOnHeap()
{
    int* pTemp = new int(20);
	cout << " *pTemp contains : " << *pTemp << "\n\n";
	cout << " pTemp mem loc is : " << pTemp << "\n\n";
	
    return pTemp;
}

void leak1()
{
    int* drip1 = new int(30);
	cout << " *drip1 contains : " << *drip1 << "\n\n";
	cout << " drip1 mem loc is : " << drip1 << "\n\n";
	delete drip1;
	drip1 = 0;
}

void leak2()
{
    int* drip2 = new int(50);
    drip2 = new int(100);
	cout << " *drip2 contains : " << *drip2 << "\n\n";
	cout << " drip2 mem loc is : " << drip2 << "\n\n";
    delete drip2;
}

