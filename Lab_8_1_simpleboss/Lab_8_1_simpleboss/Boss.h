#ifndef BOSS_h
#define BOSS_h
#include "Enemy.h"
#endif // !BOSS_H


class Boss : public Enemy
{
public:
	int m_DamageMultiplier;

	Boss();
	void SpecialAttack() const;

};
