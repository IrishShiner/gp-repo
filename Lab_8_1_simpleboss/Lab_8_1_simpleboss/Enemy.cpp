#include "Enemy.h"


Enemy::Enemy() :
	m_Damage(10)
{
	cout << "Enemy constructor called\n";
};

void Enemy::Attack() const
{
	cout << "Attack inflicts " << m_Damage << " damage points!\n";
}