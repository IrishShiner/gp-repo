#ifndef PLAYER_H
#define PLAYER_h
#include <iostream>
#include <string>


using namespace std;

class Player
{
	
public:
	Player(const string& name = "");
	string GetName() const;
	Player* GetNext() const;
	void SetNext(Player* next);
	friend ostream& operator<<(ostream& os, const Player& aPlayer);

private:
	string m_Name;
	Player* m_pNext; // pointer to next player in the list

};

#endif PLAYER_H
