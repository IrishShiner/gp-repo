#include "Boss.h"
#include "Enemy.h"

int main()
{
	cout << "Calling Attack() on Boss object through pointer to Enemy:\n";
	Enemy* pBadGuy = new Boss();
	pBadGuy->Attack();

	cout << "\n\nDeleting pointer to Enemy:\n";
	delete pBadGuy;
	pBadGuy = 0;

	Enemy* anotherBadGuy = new Boss();
	anotherBadGuy->Attack();
	getchar();

	return 0;
}