#ifndef ENEMY_H
#define ENEMY_H


class Enemy
{
public:
	Enemy();
	void Attack() const;

protected:
	int m_Damage;
};



#endif // !ENEMY_H
#pragma once
