
#include "Orc.h"
#include <iostream>
using namespace std;

Orc::Orc(int health,int speed) :
	Creature(health,speed)
{}

void Orc::Greet() const
{
	cout << "The orc grunts hello.\n";
}
void Orc::DisplayHealth() const
{
	cout << "The Orcs health is: " << m_Health << endl;
}
void Orc::DisplaySpeed() const
{
	cout << "The Orcs speed is: " << m_Speed << endl;
}