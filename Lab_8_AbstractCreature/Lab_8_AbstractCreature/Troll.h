#ifndef TROLL_H
#define TROLL_H
#include "Creature.h"
#include <iostream>
using namespace std;

class Troll : public Creature
{
public:
	Troll(int health = 200,int speed = 10);
	virtual void Greet() const;
	void DisplayHealth() const;
	void DisplaySpeed() const;
};

#endif // !Troll_H
