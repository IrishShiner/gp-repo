#include "Troll.h"
#include <iostream>
using namespace std;

Troll::Troll(int health,int speed) :
	Creature(health,speed)
{}

void Troll::Greet() const
{
	cout << "The Troll Snarls at you.\n";
}
void Troll::DisplayHealth() const
{
	cout << "The Trolls health is: " << m_Health << endl;
}
void Troll::DisplaySpeed() const
{
	cout << "The Trolls Speed is: " << m_Speed << endl;
}