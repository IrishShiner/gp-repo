#ifndef ORC_H
#define ORC_H
#include "Creature.h"
#include <iostream>
using namespace std;

class Orc : public Creature
{
public:
	Orc(int health = 120, int speed = 5);
	virtual void Greet() const;
	void DisplayHealth() const;
	void DisplaySpeed() const;
};

#endif // !ORC_H
