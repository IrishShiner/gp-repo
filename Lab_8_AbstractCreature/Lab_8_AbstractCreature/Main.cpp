#include <iostream>
#include "Creature.h"
#include "Orc.h"
#include "Troll.h"
#include <vector> // allows vectors to be used
using namespace std;



int main()
{
	Creature* oCreature = new Orc();
	oCreature->Greet();
	oCreature->DisplayHealth();
	oCreature->DisplaySpeed();

	Creature* tCreature = new Troll();
	tCreature->Greet();
	tCreature->DisplayHealth();
	tCreature->DisplaySpeed();
	
	cout << "\n\n the vector loop starts now \n\n";

	vector<Creature*> vecCreature; // creates a vector call vecCreature that can hold creature classes
	//pushes the creature objects in the vector
	vecCreature.push_back(oCreature); // added oCreature to the vector
	vecCreature.push_back(tCreature); // added tCreature to the vector
	//creates an iterator to keep track of the vectors
	std::vector<Creature*>::const_iterator iter;
	//for loop that goes from the vectors from the start to the end
	for (iter = vecCreature.begin(); iter != vecCreature.end(); iter++)
	{
		//gets the value of the interator and calls the obj at that point and 
		// calls there fuctions.
		(*iter)->Greet();
		(*iter)->DisplayHealth();
		(*iter)->DisplaySpeed();
	}

	
	getchar();



	return 0;
}
